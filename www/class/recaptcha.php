<?php 

class recaptcha  
{    
	/* 
		1) go to 
		2) enter secret key Below
	*/

	private $secret = '';

	public function getCurlData($url)
	{
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);
			curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
			$curlData = curl_exec($curl);
			curl_close($curl);
			return $curlData;
	}

	public function verify($data)
	{

	    $google_url="https://www.google.com/recaptcha/api/siteverify";
	    
	    $url = $google_url."?secret=".$secret."&response=".$data['recaptcha']."&remoteip=".$data['ip'];
	    $res = $this->getCurlData($url);
	    $res = json_decode($res, true);
	    
	    if($res['success']){ 
	    	$result = array(
	    		'success' => 'passed',
	    		);
	    	return $result;
	    }else{
	    	$result = array(
	    		'fail' => 'Failed!!',
	    		);
	        return $result;
	    }
	}
}