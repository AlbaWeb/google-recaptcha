<?php 

	/* 
		Demo purpose Only: 

		Auto load your classes...
		set your account up on: https://www.google.com/recaptcha/intro/index.html

		Best results: use on a web accessible server
	*/

	$siteKey = 'Your key here';
	$secret = 'Your key here';

	
    
    // PLACE IN FORM PROCESSING
	    $data = array(
	    //'url' => $url,
	        'ip' => $_SERVER['REMOTE_ADDR'],
	        'recaptcha' = $_POST['g-recaptcha-response'];
	    );
		$recaptcha = new recaptcha;
		$res = $recaptcha->vertify($data);
		$res = json_decode($res, true);
		if($res['success']){     
			// Success code here

		}else{
			// Fail code here 
		}
	// END PROCESSING
?>
<!DOCTYPE html>
<html>
<head>
	<title>My Form</title>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<?php 
	// echo errors
	if(empty($errors) === false) {
	    echo '<ul class="error">';
	    
	    foreach ($errors as $error)
	    {
	        echo "<li>{$error}</li>";
	    }
	    echo '</ul>';
	}
?>
            <form method="POST" action="form.php">
                	<p>
                  		<label class="control-label">Your Name</label>
                    	<input name="name" type="text" required class="form-control" id="name" placeholder="Your Name">
                    </p>
                  	<p>
                  		<label class="control-label">Email Address</label>  
                    	<input name="email" type="email" required class="form-control" id="email" placeholder="Email Address">
                    </p>
                    <p>
                    	<label class="control-label">Password</label>    
                    	<input name="pwd" type="password" required class="form-control" id="pwd">
                    </p>
                    <p>
                    	<label class="control-label">Password Confirm</label>
                    	<input name="pwd2" type="password" required class="form-control" id="pwd2">
                    </p>	
                  	<p>
                  		<label class="control-label">Capacha</label>  
                    	<div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                    </p>
					<p>
						<button class="btn btn-lg btn-primary btn-block" type="submit" name="submitBTN">Register</button>
            		</p>
            </form> 
</body>
</html>